﻿// <copyright file="AuthorsApiController.cs" company="Szabados Gergely R7L4MZ">
//     Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using MyPublicLibrary.Logic;

namespace MyPublicLibrary.Web.Controllers
{
    /// <summary>
    /// AuthorsApiController.
    /// </summary>
    public class AuthorsApiController : Controller
    {
        private IAuthorLogic logic;
        private IMapper mapper;

        /// <summary>
        /// Initializes a new instance of the <see cref="AuthorsApiController"/> class.
        /// </summary>
        /// <param name="logic">IAuthorLogic.</param>
        /// <param name="mapper">IMapper.</param>
        public AuthorsApiController(IAuthorLogic logic, IMapper mapper)
        {
            this.logic = logic;
            this.mapper = mapper;
        }

        /// <summary>
        /// Get all record.
        /// </summary>
        /// <returns>All record.</returns>
        // GET AuthorsApi/all
        [ActionName("all")]
        [HttpGet]
        public IEnumerable<Models.Author> GetAll()
        {
            var authors = logic.GetAll().ToList();
            return this.mapper.Map<IList<Data.Author>, List<Models.Author>>(authors);
        }

        /// <summary>
        /// Delete one author.
        /// </summary>
        /// <param name="id">Id of the author.</param>
        /// <returns>An Apiresult.</returns>
        // GET AuthorsApi/del/5
        [ActionName("del")]
        [HttpGet]
        public ApiResult DelOneAuthor(int id)
        {
            return new ApiResult() { OperationResult = logic.Delete((short)id) };
        }

        /// <summary>
        /// Add a new author.
        /// </summary>
        /// <param name="author">WebModel author.</param>
        /// <returns>An ApiResult.</returns>
        // POST AuhorsApi/add + author
        [HttpPost]
        [ActionName("add")]
        public ApiResult AddOneAuthor(Models.Author author)
        {
            bool success = true;
            try
            {
                logic.Create(new Data.Author { Name = author.Name, Birthdate = author.Birthdate, Email = author.Email, Phone = author.Phone, Birthplace = author.Birthplace });
            }
            catch (ArgumentException ex)
            {
                success = false;
            }

            return new ApiResult() { OperationResult = success };
        }

        /// <summary>
        /// Modify one author.
        /// </summary>
        /// <param name="author">>WebModel author.</param>
        /// <returns>An Apiresult.</returns>
        // POST AuhorsApi/mod + author
        [HttpPost]
        [ActionName("mod")]
        public ApiResult ModOneCar(Models.Author author)
        {
            return new ApiResult()
            {
                OperationResult = logic.ChangeAuthor(author.AuthorId, author.Name, author.Birthdate, author.Email, author.Phone, author.Birthplace),
            };
        }
    }
}
