﻿using Microsoft.AspNetCore.Mvc;

namespace MyPublicLibrary.Web.Controllers
{
    /// <summary>
    /// ApiResult class.
    /// </summary>
    public class ApiResult : Controller
    {
        /// <summary>
        /// Gets or sets a value indicating whether the result of the operation.
        /// </summary>
        public bool OperationResult { get; set; }
    }
}
