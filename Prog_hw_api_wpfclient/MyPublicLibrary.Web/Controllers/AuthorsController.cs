﻿// <copyright file="AuthorsController.cs" company="Szabados Gergely R7L4MZ">
//     Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>
namespace MyPublicLibrary.Web.Controllers
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using AutoMapper;
    using Microsoft.AspNetCore.Mvc;
    using MyPublicLibrary.Logic;
    using MyPublicLibrary.Web.Models;

    /// <summary>
    /// AuthorsController.
    /// </summary>
    public class AuthorsController : Controller
    {
        private IAuthorLogic logic;
        private IMapper mapper;
        private AuthorViewModel model;

        /// <summary>
        /// Initializes a new instance of the <see cref="AuthorsController"/> class.
        /// </summary>
        /// <param name="logic">IAuthorLogic.</param>
        /// <param name="mapper">IMapper.</param>
        public AuthorsController(IAuthorLogic logic, IMapper mapper)
        {
            this.logic = logic;
            this.mapper = mapper;

            this.model = new AuthorViewModel();
            this.model.EditedAuthor = new Models.Author();

            var authors = logic.GetAll().ToList();
            this.model.ListOfAuthors = mapper.Map<IList<Data.Author>, List<Models.Author>>(authors);
        }

        /// <summary>
        /// Index.
        /// </summary>
        /// <returns>A new view.</returns>
        // GET: Authors
        public IActionResult Index()
        {
            this.ViewData["editAction"] = "AddNew";
            return this.View("AuthorsIndex", this.model);
        }

        /// <summary>
        /// Details view.
        /// </summary>
        /// <param name="id">Id of the author record.</param>
        /// <returns>AuthoreDetails.cshtml view.</returns>
        // GET: Authors/Details/5
        public ActionResult Details(int id)
        {
            return this.View("AuthorsDetails", this.GetAuthor(id));
        }

        /// <summary>
        /// Remove a record.
        /// </summary>
        /// <param name="id">Id of the author record.</param>
        /// <returns>Succesfull or not Failed.</returns>
        public ActionResult Remove(int id)
        {
            this.TempData["editResult"] = "Delete FAIL - Hivatkozik rá egy másik tábla";
            if (this.logic.Delete((short)id))
            {
                this.TempData["editResult"] = "Delete OK";
            }

            return this.RedirectToAction(nameof(this.Index));
        }

        /// <summary>
        /// Edit an author.
        /// </summary>
        /// <param name="id">Id of the author record.</param>
        /// <returns>Authorsindex.cshtml view.</returns>
        // GET Authors/Edit/20
        public ActionResult Edit(int id)
        {
            this.ViewData["editAction"] = "Edit";
            this.model.EditedAuthor = this.GetAuthor(id);
            return this.View("AuthorsIndex", this.model);
        }

        /// <summary>
        /// Edit an author.
        /// </summary>
        /// <param name="author">Web.Model.Author (Formmodel).</param>
        /// <param name="editAction">An action.</param>
        /// <returns>An editresult: ok or failed.</returns>
        // POST Crud/Edit + send 1 author
        [HttpPost]
        public ActionResult Edit(Models.Author author, string editAction)
        {
            if (this.ModelState.IsValid && author != null)
            {
                this.TempData["editResult"] = "Edit OK";
                if (editAction == "AddNew")
                {
                    if (author.Email.Contains("@"))
                    {
                        try
                        {
                            this.logic.Create(new Data.Author { Name = author.Name, Birthdate = author.Birthdate, Email = author.Email, Phone = author.Phone, Birthplace = author.Birthplace });
                        }
                        catch (ArgumentException ex)
                        {
                            this.TempData["editResult"] = "Insert FAIL: " + ex.Message;
                        }
                    }
                    else
                    {
                        this.TempData["editResult"] = "Edit FAIL - Email must contains @ !";
                    }
                }
                else
                {
                    if (!this.logic.ChangeAuthor(author.AuthorId, author.Name, author.Birthdate, author.Email, author.Phone, author.Birthplace))
                    {
                        this.TempData["editResult"] = "Edit FAIL";
                    }
                }

                return this.RedirectToAction(nameof(this.Index));
            }
            else
            {
                this.ViewData["editAction"] = "Edit";
                this.model.EditedAuthor = author;
                return this.View("AuthorsIndex", this.model);
            }
        }

        private Models.Author GetAuthor(int id)
        {
            Data.Author oneauthor = this.logic.GetById((short)id);
            return this.mapper.Map<Data.Author, Models.Author>(oneauthor);
        }
    }
}
