﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using MyPublicLibrary.Web.Models;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;

namespace MyPublicLibrary.Web.Controllers
{
    /// <summary>
    /// HomeController class.
    /// </summary>
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;

        /// <summary>
        /// Initializes a new instance of the <see cref="HomeController"/> class.
        /// </summary>
        /// <param name="logger">ILogger HomeController.</param>
        public HomeController(ILogger<HomeController> logger)
        {
            _logger = logger;
        }

        /// <summary>
        /// IActionResult Index.
        /// </summary>
        /// <returns>A view.</returns>
        public IActionResult Index()
        {
            return View();
        }

        /// <summary>
        /// IActionResult Privacy.
        /// </summary>
        /// <returns>A view.</returns>
        public IActionResult Privacy()
        {
            return View();
        }

        /// <summary>
        /// IActionResult Error.
        /// </summary>
        /// <returns>A view.</returns>
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
