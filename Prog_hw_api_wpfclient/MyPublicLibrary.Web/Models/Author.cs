﻿// <copyright file="Author.cs" company="Szabados Gergely R7L4MZ">
//     Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>
namespace MyPublicLibrary.Web.Models
{
    using System;
    using System.ComponentModel.DataAnnotations;

    /// <summary>
    /// // Form Model / MVC ViewModel.
    /// </summary>
    public class Author
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Author"/> class.
        /// </summary>
        public Author()
        {
            this.Birthdate = DateTime.Now;
        }

        /// <summary>
        /// Gets or sets the id of the author.
        /// </summary>
        [Display(Name = "Author id")]
        [Required]
        public short AuthorId { get; set; }

        /// <summary>
        /// Gets or sets the name of the author.
        /// </summary>
        [Display(Name = "Author name")]
        [Required]
        [StringLength(40, MinimumLength = 5)]
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets the birthdate of the author.
        /// </summary>
        [Display(Name = "Author birthdate")]
        [Required]
        public DateTime Birthdate { get; set; }

        /// <summary>
        /// Gets or sets the email of the author.
        /// </summary>
        [Display(Name = "Author email - must contain @")]
        [Required]
        [StringLength(60, MinimumLength = 10)]
        public string Email { get; set; }

        /// <summary>
        /// Gets or sets the phonenumber of the author.
        /// </summary>
        [Display(Name = "Author phone")]
        [StringLength(11)]
        public string Phone { get; set; }

        /// <summary>
        /// Gets or sets the birthplace of the author.
        /// </summary>
        [Display(Name = "Author birthplace")]
        [StringLength(30)]
        public string Birthplace { get; set; }
    }
}
