﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MyPublicLibrary.Web.Models
{
    /// <summary>
    /// AuthorViewModel class.
    /// </summary>
    public class AuthorViewModel
    {
        /// <summary>
        /// Gets or sets the edited author.
        /// </summary>
        public Author EditedAuthor { get; set; }

        /// <summary>
        /// Gets or sets the list of the authors.
        /// </summary>
        public List<Author> ListOfAuthors { get; set; }
    }
}
