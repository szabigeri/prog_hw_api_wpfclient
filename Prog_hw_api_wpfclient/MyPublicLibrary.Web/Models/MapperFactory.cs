﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;

namespace MyPublicLibrary.Web.Models
{
    /// <summary>
    /// MapperFactory class.
    /// </summary>
    public class MapperFactory
    {
        /// <summary>
        /// CreateMapper method.
        /// </summary>
        /// <returns>A new mapper configuration.</returns>
        public static IMapper CreateMapper()
        {
            var config = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<MyPublicLibrary.Data.Author, MyPublicLibrary.Web.Models.Author>().
                ForMember(dest => dest.AuthorId, map => map.MapFrom(src => src.AuthorId)).
                ForMember(dest => dest.Name, map => map.MapFrom(src => src.Name)).
                ForMember(dest => dest.Birthdate, map => map.MapFrom(src => src.Birthdate)).
                ForMember(dest => dest.Email, map => map.MapFrom(src => src.Email)).
                ForMember(dest => dest.Phone, map => map.MapFrom(src => src.Phone)).
                ForMember(dest => dest.Birthplace, map => map.MapFrom(src => src.Birthplace));
            });
            return config.CreateMapper();
        }
    }
}
