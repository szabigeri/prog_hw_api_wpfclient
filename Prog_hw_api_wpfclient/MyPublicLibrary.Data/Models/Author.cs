﻿// <copyright file="Author.cs" company="Szabados Gergely R7L4MZ">
//     Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>
using System;

namespace MyPublicLibrary.Data
{
    /// <summary>
    /// Author class.
    /// </summary>
    public partial class Author
    {
        /// <summary>
        /// Gets or sets AuthorId.
        /// </summary>
        public short AuthorId { get; set; }

        /// <summary>
        /// Gets or sets Name.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets Birthdate.
        /// </summary>
        public DateTime Birthdate { get; set; }

        /// <summary>
        /// Gets or sets Email.
        /// </summary>
        public string Email { get; set; }

        /// <summary>
        /// Gets or sets Phone.
        /// </summary>
        public string Phone { get; set; }

        /// <summary>
        /// Gets or sets Birthplace.
        /// </summary>
        public string Birthplace { get; set; }
    }
}
