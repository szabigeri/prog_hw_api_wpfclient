﻿// <copyright file="MyPublicLibraryDbContext.cs" company="Szabados Gergely R7L4MZ">
//     Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>
using Microsoft.EntityFrameworkCore;

namespace MyPublicLibrary.Data
{
    /// <summary>
    /// MyPublicLibraryDbContext class.
    /// </summary>
    public partial class MyPublicLibraryDbContext : DbContext
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="MyPublicLibraryDbContext"/> class.
        /// </summary>
        public MyPublicLibraryDbContext()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="MyPublicLibraryDbContext"/> class.
        /// </summary>
        /// <param name="options">Database context.</param>
        public MyPublicLibraryDbContext(DbContextOptions<MyPublicLibraryDbContext> options)
            : base(options)
        {
        }

        /// <summary>
        /// Gets or sets authors.
        /// </summary>
        public virtual DbSet<Author> Authors { get; set; }

        /// <inheritdoc/>
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                optionsBuilder.UseLazyLoadingProxies()
                    .UseSqlServer("Data Source = (LocalDB)\\MSSQLLocalDB; AttachDbFilename=|DataDirectory|\\MyPublicLibraryDb.mdf; Integrated Security = True");
            }
        }

        /// <inheritdoc/>
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.HasAnnotation("Relational:Collation", "SQL_Latin1_General_CP1_CI_AS");

            modelBuilder.Entity<Author>(entity =>
            {
                entity.HasIndex(e => e.Email, "UQ__Authors__AB6E61647C4EE822")
                    .IsUnique();

                entity.Property(e => e.AuthorId)
                    .ValueGeneratedNever()
                    .HasColumnName("authorID");

                entity.Property(e => e.Birthdate)
                    .HasColumnType("datetime")
                    .HasColumnName("birthdate");

                entity.Property(e => e.Birthplace)
                    .HasMaxLength(30)
                    .IsUnicode(false)
                    .HasColumnName("birthplace");

                entity.Property(e => e.Email)
                    .IsRequired()
                    .HasMaxLength(60)
                    .IsUnicode(false)
                    .HasColumnName("email");

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(40)
                    .IsUnicode(false)
                    .HasColumnName("name");

                entity.Property(e => e.Phone)
                    .HasMaxLength(11)
                    .IsUnicode(false)
                    .HasColumnName("phone");
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
