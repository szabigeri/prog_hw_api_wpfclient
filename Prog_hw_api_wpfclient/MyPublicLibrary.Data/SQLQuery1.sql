﻿create table Authors (
	authorID smallint not null primary key,
	name varchar(40) not null, 
	birthdate datetime not null, 
	email varchar(60) not null UNIQUE,
	phone varchar(11),
	birthplace varchar(30),
	constraint emailcheck check (email like '%@%')
	);

	
insert into Authors  values (352,'Shakespeare','1850.02.10','shakespeare11@gmail.com','36209111632','London');
insert into Authors  values (166,'Molnár Ferenc', '1900.04.07','molnarferi44@gmail.com','36703219856',null);
insert into Authors  values (333,'Mikszáth Kálmán','1805.12.31','mkk943@gmail.com','36304512682',null);
insert into Authors  values (444,'Jókai Mór','1900.06.02','jokaibp@citromail.com','36203289465','Budapest');
insert into Authors  values (666,'Joanne Kathleen Rowling','1920.11.15','harrypotter@bbc.com','36708529637','Kecskemét');
insert into Authors  values (945,'Karinthy Frigyes','2010.09.12','karinthyf@freemail.com','36308425754','Nagyvárad');