﻿// <copyright file="MainVM.cs" company="Szabados Gergely R7L4MZ">
//     Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;

namespace MyPublicLibrary.WpfClient
{
    /// <summary>
    /// MainViewModel class.
    /// </summary>
    internal class MainVM : ViewModelBase
    {
        private MainLogic logic;
        private ObservableCollection<AuthorVM> allAuthors;
        private AuthorVM selectedAuthor;

        /// <summary>
        /// Initializes a new instance of the <see cref="MainVM"/> class.
        /// </summary>
        public MainVM()
        {
            this.logic = new MainLogic();

            LoadCmd = new RelayCommand(() => AllAuthors = new ObservableCollection<AuthorVM>(logic.ApiGetAuthors()));
            DelCmd = new RelayCommand(() => this.logic.ApiDelAuthor(selectedAuthor));
            AddCmd = new RelayCommand(() => this.logic.EditAuthor(null, EditorFunc));
            ModCmd = new RelayCommand(() => this.logic.EditAuthor(selectedAuthor, EditorFunc));
        }

        /// <summary>
        /// Gets or sets the selected author.
        /// </summary>
        public AuthorVM SelectedAuthor
        {
            get { return selectedAuthor; }
            set { Set(ref selectedAuthor, value); }
        }

        /// <summary>
        /// Gets or sets the authors collection.
        /// </summary>
        public ObservableCollection<AuthorVM> AllAuthors
        {
            get { return allAuthors; }
            set { Set(ref allAuthors, value); }
        }

        /// <summary>
        /// Gets or sets the function.
        /// </summary>
        public Func<AuthorVM, bool> EditorFunc { get; set; }

        /// <summary>
        /// Gets addCmd.
        /// </summary>
        public ICommand AddCmd { get; private set; }

        /// <summary>
        /// Gets modCmd.
        /// </summary>
        public ICommand ModCmd { get; private set; }

        /// <summary>
        /// Gets delCmd.
        /// </summary>
        public ICommand DelCmd { get; private set; }

        /// <summary>
        /// Gets loadCmd.
        /// </summary>
        public ICommand LoadCmd { get; private set; }
    }
}
