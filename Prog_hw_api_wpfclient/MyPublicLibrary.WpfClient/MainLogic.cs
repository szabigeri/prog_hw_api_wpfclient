﻿// <copyright file="MainLogic.cs" company="Szabados Gergely R7L4MZ">
//     Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;
using GalaSoft.MvvmLight.Messaging;

namespace MyPublicLibrary.WpfClient
{
    /// <summary>
    /// MainLogic class.
    /// </summary>
    internal class MainLogic
    {
        private string url = "http://localhost:50199/AuthorsApi/";
        private HttpClient client = new HttpClient();
        private JsonSerializerOptions jsonOptions = new JsonSerializerOptions(JsonSerializerDefaults.Web);

        /// <summary>
        /// Get all the authors using api.
        /// </summary>
        /// <returns>A list with author.</returns>
        public List<AuthorVM> ApiGetAuthors()
        {
            string json = this.client.GetStringAsync(url + "all").Result;
            var list = JsonSerializer.Deserialize<List<AuthorVM>>(json, jsonOptions);
            return list;
        }

        /// <summary>
        /// Delete authors using api.
        /// </summary>
        /// <param name="author">An Author viewmodel.</param>
        public void ApiDelAuthor(AuthorVM author)
        {
            bool success = false;
            if (author != null)
            {
                string json = this.client.GetStringAsync(url + "del/" + author.AuthorId.ToString()).Result;
                JsonDocument doc = JsonDocument.Parse(json);
                success = doc.RootElement.EnumerateObject().First().Value.GetRawText() == "true";
            }
        }

        /// <summary>
        /// Edit or create a new author.
        /// </summary>
        /// <param name="author">An author viewmodel.</param>
        /// <param name="editor">A function.</param>
        public void EditAuthor(AuthorVM author, Func<AuthorVM, bool> editor)
        {
            AuthorVM clone = new AuthorVM();
            if (author != null)
            {
                clone.CopyFrom(author);
            }

            bool? success = editor?.Invoke(clone);
            if (success != null && success == true)
            {
                if (clone.Email == null || !clone.Email.Contains('@', StringComparison.Ordinal))
                {
                    success = false;
                }
            }

            if (success == true)
            {
                if (author != null)
                {
                    success = ApiEditAuthor(clone, true);
                }
                else
                {
                    success = this.ApiEditAuthor(clone, false);
                }
            }

            SendMessage(success == true);
        }

        private bool ApiEditAuthor(AuthorVM author, bool isEditing)
        {
            if (author == null)
            {
                return false;
            }

            string myUrl = isEditing ? url + "mod" : url + "add";

            Dictionary<string, string> postData = new Dictionary<string, string>();
            if (isEditing)
            {
                postData.Add("authorId", author.AuthorId.ToString());
            }

            postData.Add("name", author.Name);
            postData.Add("birthdate", author.Birthdate.ToString());
            postData.Add("email", author.Email);
            postData.Add("phone", author.Phone);
            postData.Add("birthplace", author.Birthplace);
            string json = client.PostAsync(myUrl, new FormUrlEncodedContent(postData)).
                Result.Content.ReadAsStringAsync().Result;

            JsonDocument doc = JsonDocument.Parse(json);
            return doc.RootElement.EnumerateObject().First().Value.GetRawText() == "true";
        }

        /// <summary>
        /// Send a message about the operation. Succesfully or failed.
        /// </summary>
        /// <param name="success">True, false or null.</param>
        private void SendMessage(bool success)
        {
            string msg = success ? "Operation completed successfully" : "Operation failed";
            Messenger.Default.Send(msg, "AuthorResult");
        }
    }
}
