﻿// <copyright file="AuthorVM.cs" company="Szabados Gergely R7L4MZ">
//     Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>
using System;
using System.Linq;
using GalaSoft.MvvmLight;

namespace MyPublicLibrary.WpfClient
{
    /// <summary>
    /// Author viewmodel class.
    /// </summary>
    public class AuthorVM : ObservableObject
    {
        private int id;
        private string name;
        private DateTime birthdate;
        private string email;
        private string birthPlace;
        private string phoneNumber;

        /// <summary>
        /// Initializes a new instance of the <see cref="AuthorVM"/> class.
        /// </summary>
        public AuthorVM()
        {
            this.birthdate = DateTime.Now.Date;
        }

        /// <summary>
        /// Gets or Sets Id.
        /// </summary>
        public int AuthorId
        {
            get { return this.id; }
            set { this.Set(ref this.id, value); }
        }

        /// <summary>
        /// Gets or Sets Name.
        /// </summary>
        public string Name
        {
            get { return this.name; }
            set { this.Set(ref this.name, value); }
        }

        /// <summary>
        /// Gets or Sets Birthdate.
        /// </summary>
        public DateTime Birthdate
        {
            get { return this.birthdate; }
            set { this.Set(ref this.birthdate, value); }
        }

        /// <summary>
        /// Gets or Sets Email.
        /// </summary>
        public string Email
        {
            get { return this.email; }
            set { this.Set(ref this.email, value); }
        }

        /// <summary>
        /// Gets or Sets BirthPlace.
        /// </summary>
        public string Birthplace
        {
            get { return this.birthPlace; }
            set { this.Set(ref this.birthPlace, value); }
        }

        /// <summary>
        /// Gets or Sets PhoneNumber.
        /// </summary>
        public string Phone
        {
            get { return this.phoneNumber; }
            set { this.Set(ref this.phoneNumber, value); }
        }

        /// <summary>
        /// Copy the properties.
        /// </summary>
        /// <param name="other">Author type paramater.</param>
        public void CopyFrom(AuthorVM other)
        {
            this.GetType().GetProperties().ToList().ForEach(
                property => property.SetValue(this, property.GetValue(other)));
        }
    }
}
