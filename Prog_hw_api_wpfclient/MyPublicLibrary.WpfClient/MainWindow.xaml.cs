﻿// <copyright file="MainWindow.xaml.cs" company="Szabados Gergely R7L4MZ">
//     Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>
using System.Windows;
using GalaSoft.MvvmLight.Messaging;

namespace MyPublicLibrary.WpfClient
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml.
    /// </summary>
    public partial class MainWindow : Window
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="MainWindow"/> class.
        /// </summary>
        public MainWindow()
        {
            InitializeComponent();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            Messenger.Default.Register<string>(this, "AuthorResult", msg =>
            {
                (DataContext as MainVM).LoadCmd.Execute(null);
                MessageBox.Show(msg);
            });

            (DataContext as MainVM).EditorFunc = (author) =>
            {
                EditorWindow win = new EditorWindow(author);
                return win.ShowDialog() == true;
            };
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            Messenger.Default.Unregister(this);
        }
    }
}
