﻿// <copyright file="BaseRepository.cs" company="Szabados Gergely R7L4MZ">
//     Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>
namespace MyPublicLibrary.Repository
{
    using System.Linq;
    using Microsoft.EntityFrameworkCore;

    /// <summary>
    /// BaseRepository class.
    /// </summary>
    /// <typeparam name="T">T type.</typeparam>
    public abstract class BaseRepository<T> : IRepository<T>
        where T : class
    {
        /// <summary>
        /// Database context.
        /// </summary>
        protected DbContext ctx;

        /// <summary>
        /// Initializes a new instance of the <see cref="BaseRepository{T}"/> class.
        /// Repository constructor.
        /// </summary>
        /// <param name="ctx">Database context.</param>
        protected BaseRepository(DbContext ctx)
        {
            this.ctx = ctx;
        }

        /// <summary>
        /// Get all method.
        /// </summary>
        /// <returns>All of the T type.</returns>
        public IQueryable<T> GetAll()
        {
            return this.ctx.Set<T>();
        }
    }
}
