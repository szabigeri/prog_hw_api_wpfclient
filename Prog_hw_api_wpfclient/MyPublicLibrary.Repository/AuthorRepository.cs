﻿// <copyright file="AuthorRepository.cs" company="Szabados Gergely R7L4MZ">
//     Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>
namespace MyPublicLibrary.Repository
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Microsoft.EntityFrameworkCore;
    using MyPublicLibrary.Data;

    /// <summary>
    /// AuthorRepository.
    /// </summary>
    public class AuthorRepository : BaseRepository<Author>, IAuthorsRepository
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="AuthorRepository"/> class.
        /// </summary>
        /// <param name="ctx">DbContext context.</param>
        public AuthorRepository(DbContext ctx)
            : base(ctx)
        {
        }

        /// <summary>
        /// Create a new author.
        /// </summary>
        /// <param name="author">New author.</param>
        public void Create(Author author)
        {
            if (author.AuthorId == 0)
            {
                var allauthor = this.GetAll();
                short ret = 100;
                List<short> allid = new List<short>();
                foreach (var item in allauthor)
                {
                    allid.Add(item.AuthorId);
                }

                do
                {
                    ret++;
                }
                while (allid.Contains(ret));
                author.AuthorId = ret;
            }

            this.ctx.Set<Author>().Add(author);
            this.ctx.SaveChanges();
        }

        /// <summary>
        /// Change the parametres of a choosen Author.
        /// </summary>
        /// <param name="id">Id of the author.</param>
        /// <param name="name">name of the author.</param>
        /// <param name="birthdate">Birthdate of the author.</param>
        /// <param name="email">Email of the author.</param>
        /// <param name="phone">Phone of the author.</param>
        /// <param name="birthplace">Birtplace of the author.</param>
        /// <returns>True or false.</returns>
        public bool ChangeAuthor(short id, string name, DateTime birthdate, string email, string phone, string birthplace)
        {
            Author author = this.GetById(id);
            if (author == null || birthdate.Year < 1800 || !email.Contains('@', StringComparison.OrdinalIgnoreCase))
            {
                return false;
            }

            author.Name = name;
            author.Birthdate = birthdate;
            author.Email = email;
            author.Phone = phone;
            author.Birthplace = birthplace;
            this.ctx.SaveChanges();
            return true;
        }

        /// <summary>
        /// Delete an author by id.
        /// </summary>
        /// <param name="id">authorID.</param>
        /// <returns>True or false.</returns>
        public bool Delete(short id)
        {
            Author someAuthor = this.GetById(id);
            try
            {
                this.ctx.Remove(this.GetById(id));
                this.ctx.SaveChanges();
            }
            catch (Microsoft.EntityFrameworkCore.DbUpdateException)
            {
                this.ctx.Entry<Author>(someAuthor).State = EntityState.Detached;
            }

            return this.GetById(id) == null;
        }

        /// <summary>
        /// Select an author by id.
        /// </summary>
        /// <param name="id">AuthorID.</param>
        /// <returns>Return whith a szerzok type.</returns>
        public Author GetById(short id)
        {
            return this.GetAll().SingleOrDefault(x => x.AuthorId == id);
        }

        /// <summary>
        /// UPDATE email by id.
        /// </summary>
        /// <param name="id">authorID.</param>
        /// <param name="newemail">New email of the author.</param>
        public void Update_email(short id, string newemail)
        {
            Author author = this.GetById(id);
            author.Email = newemail;
            this.ctx.SaveChanges();
        }

        /// <summary>
        /// UPDATE name by id.
        /// </summary>
        /// <param name="id">authorID.</param>
        /// <param name="newname">New name of the author.</param>
        public void Update_name(short id, string newname)
        {
            Author author = this.GetById(id);
            author.Name = newname;
            this.ctx.SaveChanges();
        }

        /// <summary>
        /// UPDATE birthdate by id.
        /// </summary>
        /// <param name="id">authorID.</param>
        /// <param name="newdate">New birthdate of the author.</param>
        public void Update_birtdate(short id, DateTime newdate)
        {
            Author author = this.GetById(id);
            author.Birthdate = newdate;
            this.ctx.SaveChanges();
        }

        /// <summary>
        /// UPDATE author_id by id.
        /// </summary>
        /// <param name="id">authorID.</param>
        /// <param name="newid">New authorID.</param>
        public void Update_authorid(short id, short newid)
        {
            Author oldauthor = this.GetById(id);
            Author newauthor = new Author()
            {
                AuthorId = newid,
                Name = oldauthor.Name,
                Birthdate = oldauthor.Birthdate,
                Birthplace = oldauthor.Birthplace,
                Email = oldauthor.Email,
                Phone = oldauthor.Phone,
            };
            this.Delete(oldauthor.AuthorId);
            this.Create(newauthor);
            this.ctx.SaveChanges();
        }

        /// <summary>
        /// UPDATE birthplace by id.
        /// </summary>
        /// <param name="id">authorID.</param>
        /// <param name="newbirthplace">New phone number of the author.</param>
        public void Update_birthplace(short id, string newbirthplace)
        {
            Author author = this.GetById(id);
            author.Birthplace = newbirthplace;
            this.ctx.SaveChanges();
        }

        /// <summary>
        /// UPDATE phone number by id.
        /// </summary>
        /// <param name="id">authorID.</param>
        /// <param name="newphone">New phone number of the author.</param>
        public void Update_phone(short id, string newphone)
        {
            Author author = this.GetById(id);
            author.Phone = newphone;
            this.ctx.SaveChanges();
        }
    }
}