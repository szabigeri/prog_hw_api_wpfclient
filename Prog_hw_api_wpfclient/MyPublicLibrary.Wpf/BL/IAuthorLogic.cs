﻿// <copyright file="IAuthorLogic.cs" company="Szabados Gergely R7L4MZ">
//     Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>
namespace MyPublicLibrary.Wpf.BL
{
    using System.Collections.Generic;
    using MyPublicLibrary.Wpf.Data;

    /// <summary>
    /// IAuthorLogic interface.
    /// </summary>
    internal interface IAuthorLogic
    {
        /// <summary>
        /// Add an author to the last and to the databese.
        /// </summary>
        /// <param name="list">List which contains authors.</param>
        void AddAuthor(IList<WpfAuthor> list);

        /// <summary>
        /// Modify an author.
        /// </summary>
        /// <param name="authorToModify">The author that we want to edit.</param>
        void ModAuthor(WpfAuthor authorToModify);

        /// <summary>
        /// Delete an author.
        /// </summary>
        /// <param name="list">A list that contains authors.</param>
        /// <param name="author">An author.</param>
        void DelAuthor(IList<WpfAuthor> list, WpfAuthor author);

        /// <summary>
        /// A method which return with the all author.
        /// </summary>
        /// <returns>List that contains authors.</returns>
        IList<WpfAuthor> GetAllAuthor();
    }
}