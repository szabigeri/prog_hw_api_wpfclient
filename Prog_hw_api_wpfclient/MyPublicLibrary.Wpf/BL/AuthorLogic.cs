﻿// <copyright file="AuthorLogic.cs" company="Szabados Gergely R7L4MZ">
//     Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>
namespace MyPublicLibrary.Wpf.BL
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using GalaSoft.MvvmLight.Messaging;
    using MyPublicLibrary.Data;
    using MyPublicLibrary.Repository;
    using MyPublicLibrary.Wpf.Data;

    /// <summary>
    /// AuthorLogic class.
    /// </summary>
    internal class AuthorLogic : IAuthorLogic
    {
        private Logic.AuthorLogic authorlogic;
        private IEditorService editorService;
        private IMessenger messengerService;

        /// <summary>
        /// Initializes a new instance of the <see cref="AuthorLogic"/> class.
        /// </summary>
        /// <param name="editorService">IEditorService service.</param>
        /// <param name="messengerService">IMessenger service.</param>
        public AuthorLogic(IEditorService editorService, IMessenger messengerService)
        {
            this.editorService = editorService;
            this.messengerService = messengerService;
            MyPublicLibraryDbContext ctx = new MyPublicLibraryDbContext();
            this.authorlogic = new Logic.AuthorLogic(new AuthorRepository(ctx));
        }

        /// <summary>
        /// Add an author to the last and to the databese.
        /// </summary>
        /// <param name="list">List which contains authors.</param>
        public void AddAuthor(IList<WpfAuthor> list)
        {
            WpfAuthor newAuthor = new WpfAuthor();
            if (this.editorService.EditAuthor(newAuthor) == true)
            {
                string email = newAuthor.Email;
                if (newAuthor.Name == null)
                {
                    this.messengerService.Send("ADD FAILED -- Név megadása kötelző", "LogicResult");
                }
                else if (email == null || !email.Contains("@", StringComparison.Ordinal))
                {
                    this.messengerService.Send("ADD FAILED - Hibás email formátum: hiányzik a @", "LogicResult");
                }
                else if (this.IsEmailUsed(email))
                {
                    this.messengerService.Send("ADD FAILED -- Email must be UNIQUE", "LogicResult");
                }
                else
                {
                    Author convertedauthor = new Author()
                    {
                        Name = newAuthor.Name,
                        Birthdate = newAuthor.Birthdate,
                        Email = newAuthor.Email,
                        Birthplace = newAuthor.BirthPlace,
                        Phone = newAuthor.PhoneNumber,
                    };
                    this.authorlogic.Create(convertedauthor);
                    list.Add(newAuthor);
                    this.messengerService.Send("ADD OK", "LogicResult");
                }
            }
            else
            {
                this.messengerService.Send("ADD CANCEL", "LogicResult");
            }
        }

        /// <summary>
        /// Delete an author.
        /// </summary>
        /// <param name="list">A list that contains authors.</param>
        /// <param name="author">An author.</param>
        public void DelAuthor(IList<WpfAuthor> list, WpfAuthor author)
        {
            if (author != null)
            {
                if (this.authorlogic.Delete(short.Parse(author.Id)))
                {
                    list.Remove(author);
                    this.messengerService.Send("DELETE OK", "LogicResult");
                }
                else
                {
                    this.messengerService.Send("DELETE FAILED", "LogicResult");
                }
            }
            else
            {
                this.messengerService.Send("DELETE FAILED", "LogicResult");
            }
        }

        /// <summary>
        /// Modify an author.
        /// </summary>
        /// <param name="authorToModify">The author that we want to edit.</param>
        public void ModAuthor(WpfAuthor authorToModify)
        {
            if (authorToModify == null)
            {
                this.messengerService.Send("EDIT FAILED", "LogicResult");
                return;
            }

            WpfAuthor clone = new WpfAuthor();
            clone.CopyFrom(authorToModify);
            if (this.editorService.EditAuthor(clone) == true)
            {
                string email = clone.Email;
                if (!email.Contains("@", StringComparison.Ordinal))
                {
                    this.messengerService.Send("EDIT FAILED - Hibás email formátum: hiányzik a @", "LogicResult");
                }
                else if (authorToModify.Email != clone.Email && this.IsEmailUsed(email))
                {
                    this.messengerService.Send("EDIT FAILED -- Email must be UNIQUE", "LogicResult");
                }
                else if (clone.Id == null)
                {
                    this.messengerService.Send("EDIT FAILED - az ID nem lehet üres", "LogicResult");
                }
                else
                {
                    this.authorlogic.Update_name(short.Parse(authorToModify.Id), clone.Name);
                    this.authorlogic.Update_email(short.Parse(authorToModify.Id), clone.Email);
                    this.authorlogic.Update_birthdate(short.Parse(authorToModify.Id), clone.Birthdate);
                    this.authorlogic.Update_BirthPlace(short.Parse(authorToModify.Id), clone.BirthPlace);
                    this.authorlogic.Update_phone(short.Parse(authorToModify.Id), clone.PhoneNumber);
                    authorToModify.CopyFrom(clone);
                    this.messengerService.Send("MODIFY OK", "LogicResult");
                }
            }
            else
            {
                this.messengerService.Send("MODIFY CANCEL", "LogicResult");
            }
        }

        /// <summary>
        /// A method which return with the all author.
        /// </summary>
        /// <returns>List that contains authors.</returns>
        public IList<WpfAuthor> GetAllAuthor()
        {
            List<Author> allauthor = this.authorlogic.GetAll().ToList();
            IList<WpfAuthor> ret = new List<WpfAuthor>();
            foreach (Author item in allauthor)
            {
                ret.Add(new WpfAuthor { Id = item.AuthorId.ToString(), Name = item.Name, Birthdate = item.Birthdate, Email = item.Email, BirthPlace = item.Birthplace, PhoneNumber = item.Phone });
            }

            return ret;
        }

        /// <summary>
        /// Return with true if the email is used.
        /// </summary>
        /// <param name="email">We want check this.</param>
        /// <returns>True or false.</returns>
        private bool IsEmailUsed(string email)
        {
            IQueryable<Author> allauthor = this.authorlogic.GetAll();
            List<string> allid = new List<string>();
            foreach (Author item in allauthor)
            {
                allid.Add(item.Email);
            }

            return allid.Contains(email);
        }
    }
}