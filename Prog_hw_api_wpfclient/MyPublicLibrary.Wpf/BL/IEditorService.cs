﻿// <copyright file="IEditorService.cs" company="Szabados Gergely R7L4MZ">
//     Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>
namespace MyPublicLibrary.Wpf.BL
{
    using MyPublicLibrary.Wpf.Data;

    /// <summary>
    /// IEditorService interface.
    /// </summary>
    internal interface IEditorService
    {
        /// <summary>
        /// Check the parameter type (Author).
        /// </summary>
        /// <param name="a">Author type.</param>
        /// <returns>True or false.</returns>
        bool EditAuthor(WpfAuthor a);
    }
}