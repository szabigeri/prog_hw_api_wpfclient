﻿// <copyright file="MyIoc.cs" company="Szabados Gergely R7L4MZ">
//     Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>namespace MyPublicLibrary.Wpf
namespace MyPublicLibrary.Wpf
{
    using CommonServiceLocator;
    using GalaSoft.MvvmLight.Ioc;

    /// <summary>
    /// MyIoc class.
    /// </summary>
    internal class MyIoc : SimpleIoc, IServiceLocator
    {
        /// <summary>
        /// Gets Instance.
        /// </summary>
        public static MyIoc Instance { get; private set; } = new MyIoc();
    }
}