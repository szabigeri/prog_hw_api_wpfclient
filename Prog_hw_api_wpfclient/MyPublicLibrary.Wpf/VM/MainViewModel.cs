﻿// <copyright file="MainViewModel.cs" company="Szabados Gergely R7L4MZ">
//     Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>
namespace MyPublicLibrary.Wpf.VM
{
    using System;
    using System.Collections.ObjectModel;
    using System.Windows.Input;
    using CommonServiceLocator;
    using GalaSoft.MvvmLight;
    using GalaSoft.MvvmLight.Command;
    using MyPublicLibrary.Wpf.BL;
    using MyPublicLibrary.Wpf.Data;

    /// <summary>
    /// MainViewModel class.
    /// </summary>
    internal class MainViewModel : ViewModelBase
    {
        private IAuthorLogic logic;
        private ObservableCollection<WpfAuthor> allAuthor;
        private WpfAuthor selectedauthor;

        /// <summary>
        /// Initializes a new instance of the <see cref="MainViewModel"/> class.
        /// </summary>
        public MainViewModel()
            : this(IsInDesignModeStatic ? null : ServiceLocator.Current.GetInstance<IAuthorLogic>())
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="MainViewModel"/> class.
        /// </summary>
        /// <param name="logic">IAuthorLogic interface.</param>
        public MainViewModel(IAuthorLogic logic)
        {
            this.logic = logic;
            this.AllAuthor = new ObservableCollection<WpfAuthor>();

            if (this.IsInDesignMode)
            {
                WpfAuthor p2 = new WpfAuthor() { Name = "Wild Bill 2", PhoneNumber = "0123456789" };
                WpfAuthor p3 = new WpfAuthor() { Name = "Wild Bill 3" };
                this.AllAuthor.Add(p2);
                this.AllAuthor.Add(p3);
            }

            WpfAuthor p1 = new WpfAuthor() { Birthdate = DateTime.Now.Date };

            this.AddCmd = new RelayCommand(() => this.logic.AddAuthor(this.AllAuthor));
            this.ModCmd = new RelayCommand(() => this.logic.ModAuthor(this.SelectedAuthor));
            this.DelCmd = new RelayCommand(() => this.logic.DelAuthor(this.AllAuthor, this.SelectedAuthor));
            this.LoadCmd = new RelayCommand(() => this.AllAuthor = new ObservableCollection<WpfAuthor>(this.logic.GetAllAuthor()));
        }

        /// <summary>
        /// Gets or Sets the collection of the authors..
        /// </summary>
        public ObservableCollection<WpfAuthor> AllAuthor
        {
            get { return this.allAuthor; }
            set { this.Set(ref this.allAuthor, value); }
        }

        /// <summary>
        /// Gets or Sets the selected author.
        /// </summary>
        public WpfAuthor SelectedAuthor
        {
            get { return this.selectedauthor; }
            set { this.Set(ref this.selectedauthor, value); }
        }

        /// <summary>
        /// Gets addCmd.
        /// </summary>
        public ICommand AddCmd { get; private set; }

        /// <summary>
        /// Gets modCmd.
        /// </summary>
        public ICommand ModCmd { get; private set; }

        /// <summary>
        /// Gets delCmd.
        /// </summary>
        public ICommand DelCmd { get; private set; }

        /// <summary>
        /// Gets loadCmd.
        /// </summary>
        public ICommand LoadCmd { get; private set; }
    }
}