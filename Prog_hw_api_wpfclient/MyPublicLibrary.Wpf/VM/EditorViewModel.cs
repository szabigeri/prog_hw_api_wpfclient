﻿// <copyright file="EditorViewModel.cs" company="Szabados Gergely R7L4MZ">
//     Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>
namespace MyPublicLibrary.Wpf.VM
{
    using System;
    using GalaSoft.MvvmLight;
    using MyPublicLibrary.Wpf.Data;

    /// <summary>
    /// EditorViewModel class.
    /// </summary>
    internal class EditorViewModel : ViewModelBase
    {
        private WpfAuthor author;

        /// <summary>
        /// Initializes a new instance of the <see cref="EditorViewModel"/> class.
        /// </summary>
        public EditorViewModel()
        {
            this.author = new WpfAuthor();
            if (this.IsInDesignMode)
            {
                this.author.Id = "101";
                this.author.Name = "Unknown Bill XXX";
                this.author.Birthdate = DateTime.Now;
                this.author.Email = "asd@dfg.hu";
                this.author.PhoneNumber = "0123456789";
            }
        }

        /// <summary>
        /// Gets or Sets author.
        /// </summary>
        public WpfAuthor Author
        {
            get { return this.author; }
            set { this.Set(ref this.author, value); }
        }
    }
}