﻿// <copyright file="EditorWindow.xaml.cs" company="Szabados Gergely R7L4MZ">
//     Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>
namespace MyPublicLibrary.Wpf
{
    using System.Windows;
    using MyPublicLibrary.Wpf.Data;
    using MyPublicLibrary.Wpf.VM;

    /// <summary>
    /// Interaction logic for EditorWindow.xaml.
    /// </summary>
    public partial class EditorWindow : Window
    {
        /// <summary>
        /// EditorViewModel type member data.
        /// </summary>
        private EditorViewModel vM;

        /// <summary>
        /// Initializes a new instance of the <see cref="EditorWindow"/> class.
        /// </summary>
        public EditorWindow()
        {
            this.InitializeComponent();
            this.vM = this.FindResource("VM") as EditorViewModel;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="EditorWindow"/> class.
        /// </summary>
        /// <param name="oldAuthor">Author type parameter.</param>
        public EditorWindow(WpfAuthor oldAuthor)
            : this()
        {
            this.vM.Author = oldAuthor;
        }

        /// <summary>
        /// Gets vm.Author.
        /// </summary>
        public WpfAuthor Author { get => this.vM.Author; }

        private void OkClick(object sender, RoutedEventArgs e)
        {
            this.DialogResult = true;
        }

        private void CancelClick(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
        }

        private void Phoennumber_textchecker(object sender, System.Windows.Input.TextCompositionEventArgs e)
        {
            e.Handled = !char.IsDigit(e.Text[0]);
        }
    }
}