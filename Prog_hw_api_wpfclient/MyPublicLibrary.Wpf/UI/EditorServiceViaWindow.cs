﻿// <copyright file="EditorServiceViaWindow.cs" company="Szabados Gergely R7L4MZ">
//     Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>
namespace MyPublicLibrary.Wpf.UI
{
    using MyPublicLibrary.Wpf.BL;
    using MyPublicLibrary.Wpf.Data;

    /// <summary>
    /// EditorServiceViaWindow class.
    /// </summary>
    internal class EditorServiceViaWindow : IEditorService
    {
        /// <summary>
        /// Check the parameter type (Author).
        /// </summary>
        /// <param name="a">Author type.</param>
        /// <returns>True or false.</returns>
        public bool EditAuthor(WpfAuthor a)
        {
            EditorWindow win = new EditorWindow(a);
            return win.ShowDialog() ?? false;
        }
    }
}