﻿// <copyright file="AuthorLogic.cs" company="Szabados Gergely R7L4MZ">
//     Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>
namespace MyPublicLibrary.Logic
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using MyPublicLibrary.Data;
    using MyPublicLibrary.Repository;

    /// <summary>
    /// AuthorLogic class.
    /// </summary>
    public class AuthorLogic : IAuthorLogic
    {
        private IAuthorsRepository repo;

        /// <summary>
        /// Initializes a new instance of the <see cref="AuthorLogic"/> class.
        /// </summary>
        /// <param name="repository">IAuthorsRepository.</param>
        public AuthorLogic(IAuthorsRepository repository)
        {
            this.repo = repository;
        }

        /// <summary>
        /// Create and then insert a new author.
        /// </summary>
        /// <param name="author">New author.</param>
        public void Create(Author author)
        {
            this.repo.Create(author);
        }

        /// <summary>
        /// Delete an author by id.
        /// </summary>
        /// <param name="id">szerzoID.</param>
        /// <returns>True or false.</returns>
        public bool Delete(short id)
        {
           return this.repo.Delete(id);
        }

        /// <summary>
        /// Select all from szerzok.
        /// </summary>
        /// <returns>IQueryable type:szerzok.</returns>
        public IQueryable<Author> GetAll()
        {
            return this.repo.GetAll();
        }

        /// <summary>
        /// Select from szerzok(authors) by id.
        /// </summary>
        /// <param name="id">szerzoID.</param>
        /// <returns>Return an author by id.</returns>
        public Author GetById(short id)
        {
            return this.repo.GetById(id);
        }

        /// <summary>
        /// UPDATE the current email to the newemail.
        /// </summary>
        /// <param name="id">szerzoID.</param>
        /// <param name="newemail">New email of the author.</param>
        public void Update_email(short id, string newemail)
        {
            this.repo.Update_email(id, newemail);
        }

        /// <summary>
        /// UPDATE the current name to the newnev.
        /// </summary>
        /// <param name="id">szerzoID.</param>
        /// <param name="newname">New name of the author.</param>
        public void Update_name(short id, string newname)
        {
            this.repo.Update_name(id, newname);
        }

        /// <summary>
        /// UPDATE the current birtdate to the newdatum.
        /// </summary>
        /// <param name="id">szerzoID.</param>
        /// <param name="newdate">New birthdate of the author.</param>
        public void Update_birthdate(short id, DateTime newdate)
        {
            this.repo.Update_birtdate(id, newdate);
        }

        /// <summary>
        /// UPDATE the current author id to the newid.
        /// </summary>
        /// <param name="id">szerzoID.</param>
        /// <param name="newid">New authorID.</param>
        public void Update_authorid(short id, short newid)
        {
            this.repo.Update_authorid(id, newid);
        }

        /// <summary>
        /// UPDATE the current birthplace to the newszulhely.
        /// </summary>
        /// <param name="id">szerzoID.</param>
        /// <param name="newbirthplace">New birthplace of the author.</param>
        public void Update_BirthPlace(short id, string newbirthplace)
        {
            this.repo.Update_birthplace(id, newbirthplace);
        }

        /// <summary>
        /// UPDATE the current phone number to the newtelefon.
        /// </summary>
        /// <param name="id">szerzoID.</param>
        /// <param name="newphone">New phone number of the author.</param>
        public void Update_phone(short id, string newphone)
        {
            this.repo.Update_phone(id, newphone);
        }

        /// <summary>
        /// Change the parametres of a choosen Author.
        /// </summary>
        /// <param name="id">Id of the author.</param>
        /// <param name="name">name of the author.</param>
        /// <param name="birthdate">Birthdate of the author.</param>
        /// <param name="email">Email of the author.</param>
        /// <param name="phone">Phone of the author.</param>
        /// <param name="birthplace">Birtplace of the author.</param>
        /// <returns>True or false.</returns>
        public bool ChangeAuthor(short id, string name, DateTime birthdate, string email, string phone, string birthplace)
        {
            return this.repo.ChangeAuthor(id, name, birthdate, email, phone, birthplace);
        }
    }
}