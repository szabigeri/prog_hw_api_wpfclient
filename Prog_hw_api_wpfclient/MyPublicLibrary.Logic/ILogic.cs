﻿// <copyright file="ILogic.cs" company="Szabados Gergely R7L4MZ">
//     Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>
namespace MyPublicLibrary.Logic
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using MyPublicLibrary.Data;

    /// <summary>
    /// ILogic interface.
    /// </summary>
    /// <typeparam name="T">Type og the Entity.</typeparam>
    public interface ILogic<T>
        where T : class
    {
        /// <summary>
        /// Select all.
        /// </summary>
        /// <returns>>A collection which contains T type data.</returns>
        IQueryable<T> GetAll();
    }

    /// <summary>
    /// ISzerzokLogic interface.
    /// </summary>
    public interface IAuthorLogic : ILogic<Author>
    {
        /// <summary>
        /// Select an author by id.
        /// </summary>
        /// <param name="id">ID of the author.</param>
        /// <returns>Return whith a szerzok type.</returns>
        Author GetById(short id);

        /// <summary>
        /// CREATE a new author.
        /// </summary>
        /// <param name="author">New author.</param>
        void Create(Author author);

        /// <summary>
        /// Change the parametres of a choosen Author.
        /// </summary>
        /// <param name="id">Id of the author.</param>
        /// <param name="name">name of the author.</param>
        /// <param name="birthdate">Birthdate of the author.</param>
        /// <param name="email">Email of the author.</param>
        /// <param name="phone">Phone of the author.</param>
        /// <param name="birthplace">Birtplace of the author.</param>
        /// <returns>True or false.</returns>
        bool ChangeAuthor(short id, string name, DateTime birthdate, string email, string phone, string birthplace);

        /// <summary>
        /// UPDATE author_id by id.
        /// </summary>
        /// <param name="id">szerzoID.</param>
        /// <param name="newid">New authorID.</param>
        void Update_authorid(short id, short newid);

        /// <summary>
        /// UPDATE name by id.
        /// </summary>
        /// <param name="id">szerzoID.</param>
        /// <param name="newname">New name of the author.</param>
        void Update_name(short id, string newname);

        /// <summary>
        /// UPDATE birthdate by id.
        /// </summary>
        /// <param name="id">szerzoID.</param>
        /// <param name="newdate">New birthdate of the author.</param>
        void Update_birthdate(short id, DateTime newdate);

        /// <summary>
        /// UPDATE email by id.
        /// </summary>
        /// <param name="id">szerzoID.</param>
        /// <param name="newemail">New email of the author.</param>
        void Update_email(short id, string newemail);

        /// <summary>
        /// UPDATE phone number by id.
        /// </summary>
        /// <param name="id">szerzoID.</param>
        /// <param name="newphone">New phone number of the author.</param>
        void Update_phone(short id, string newphone);

        /// <summary>
        /// UPDATE birthplace by id.
        /// </summary>
        /// <param name="id">szerzoID.</param>
        /// <param name="newbirthplace">New birthplace of the author.</param>
        void Update_BirthPlace(short id, string newbirthplace);

        /// <summary>
        /// Delete an author by id.
        /// </summary>
        /// <param name="id">szerzoID.</param>
        /// <returns>True or false.</returns>
        bool Delete(short id);
    }
}