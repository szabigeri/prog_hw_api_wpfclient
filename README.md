Hello! This is my university homework.
Data stored in a database can be modified using Wpf + WebApi or in a browser (ASP).

There is a table in the database that contains the following:
ID, 
Name, 
Date of birth, 
Email, 
Phone number, 
Place of birth

Layers:
Data
Repository
Logic
Web (ASP) + WebApi
WpfClient

Start multiple projects: Web + WpfClient